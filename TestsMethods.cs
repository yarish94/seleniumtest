﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace Selenium_Tests
{
    public partial class Tests
    {
        public void Authorization()
        {
            Authorization(new User());
        }
        public void Authorization(User user)
        {
            Driver.Navigate().GoToUrl("https://localhost:44356/");
            Driver.FindElement(By.XPath("//*[@id='cookieConsent']/div/div[2]/div/button")).Click();
            Driver.FindElement(By.XPath("/html/body/nav[1]/div/div[2]/ul/li/a")).Click();
            Driver.FindElement(By.XPath("//*[@id='Input_Email']")).SendKeys(user.Login);
            Driver.FindElement(By.XPath("//*[@id='Input_Password']")).SendKeys(user.Password);
            Driver.FindElement(By.XPath("/html/body/div/div/div/section/form/div[5]/button")).Click();
            var actualUserName = Driver.FindElement(By.XPath("//*[@id='UserName']")).Text;
            Assert.AreEqual(actualUserName, user.FullName);
        }

        [TestCleanup]
        public void CleanUp()
        {
            Driver.Quit();
        }
    }
}
