﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Selenium_Tests
{
    public class User
    {
        public User()
        {
            login = "Vadim.Yarysh";
            password = "Y102663$";
            fullName = "Ярыш Вадим Вячеславович";
            machineName = "R00YARISHVV";
            isPersonal = true;
        }
        #region public Properties
        public string Login => login;
        public string Password => password;
        public string FullName => fullName;
        public string MachineName => machineName;
        public bool IsPersonal => isPersonal;
        #endregion
        #region private Fields
        private string login;
        private string password;
        private string fullName;
        private string machineName;
        private bool isPersonal;
        #endregion
    }
}
