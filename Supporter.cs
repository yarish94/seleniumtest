﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;

namespace Selenium_Tests
{
    public static class Supporter
    {
        public static void KillProcess(string name = "chomedriver")
        {
            if (Process.GetProcessesByName(name).Length == 0) return;
            try
            {
                foreach (Process proc in Process.GetProcessesByName(name))
                {
                    proc.Kill();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
