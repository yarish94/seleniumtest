using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Selenium_Tests
{
    [TestClass]
    public partial class Tests
    {
        private IWebDriver driver = new ChromeDriver();
        internal IWebDriver Driver => driver ?? new ChromeDriver();
        [TestMethod]
        public void OpenMainWebPage()
        {
            Driver.Navigate().GoToUrl("https://localhost:44356/");
            var openedUrl = Driver.Url;
            Assert.AreEqual("https://localhost:44356/", openedUrl);
            Driver.Quit();
        }


        [TestMethod]
        public void GoToRunnersPage()
        {
            Authorization();
            Driver.Navigate().GoToUrl("https://localhost:44356/Runners");
            Driver.FindElement(By.XPath("/html/body/div/div[1]/h3"));
            Driver.FindElement(By.XPath("//*[@id='action' and @title='������������� ���']"));
            Driver.FindElement(By.XPath("//*[@id='action' and @title='�������� ���']"));
            Driver.FindElement(By.XPath("//*[@id='action' and @title='���������� ���']"));
            Driver.FindElement(By.XPath("//*[@id='action' and @title='��������� ���']"));
            Driver.FindElement(By.XPath("/html/body/div/div[1]/a"));
            Driver.FindElement(By.XPath("//*[@id='runner_find_string']"));
            Driver.FindElement(By.XPath("//*[@id='runner_find_type']"));
            Driver.FindElement(By.XPath("//*[@id='runner_find_status']"));
            Driver.FindElement(By.XPath("//*[@id='cell_refs']/a"));
            Driver.FindElement(By.XPath("//*[@id='cell_type_id']/a"));
            Driver.FindElement(By.XPath("//*[@id='table_test_runners']/thead/tr"));
        }

        [TestMethod]
        public void CheckRunnerOnRunnerPage()
        {
            User user = new User();
            Authorization(user);
            Driver.Navigate().GoToUrl("https://localhost:44356/Runners");
            Driver.FindElement(By.XPath("//*[@id='cell_type_id']/a")).Click();
            var tableRows = Driver.FindElements(By.XPath("//*[@id='table_test_runners']/tbody/tr"));
            Assert.AreNotEqual(0, tableRows.Count);
            List<IWebElement> finalList = new List<IWebElement>();
            foreach (var row in tableRows)
            {
                try
                {
                    finalList.Add(row.FindElement(By.LinkText(user.MachineName)));
                    return;
                }
                catch { continue; }
            }
            Driver.Quit();
            throw new Exception("��� ������� �� ������� � �������");
        }

        [TestMethod]
        public void CheckRunnerForDebug()
        {
            User user = new User();
            Authorization(user);
            Driver.Navigate().GoToUrl("https://localhost:44356/Runners");
            var elType = Driver.FindElement(By.Id("runner_find_status"));
            var acts = new Actions(Driver);
            acts.Click(elType).SendKeys(Keys.ArrowDown).SendKeys(Keys.ArrowDown).SendKeys(Keys.Enter)
                .Build().Perform();
            Driver.FindElement(By.XPath("//*[@id='cell_type_id']/a")).Click();
            var tableRows = Driver.FindElements(By.XPath("//*[@id='table_test_runners']/tbody/tr"));
            Assert.AreNotEqual(0, tableRows.Count);
            List<IWebElement> finalList = new List<IWebElement>();
            foreach (var row in tableRows)
            {
                try
                {
                    finalList.Add(row.FindElement(By.LinkText(user.MachineName)));
                    return;
                }
                catch { continue; }
            }
            Driver.Quit();
            throw new Exception("��� ������� �� ������� � �������");
        }

        [TestMethod]
        [Obsolete]
        public void CreateAnEmptyElement()
        {
            User user = new User();
            Authorization(user);
            Driver.FindElement(By.XPath(".//a[text()='��������']")).Click();
            Driver.FindElement(By.XPath(".//a/span[text() = '�������']")).Click();
            Driver.FindElement(By.XPath("//*[@id='case-builder']/button")).Click();
            var waiter = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            waiter.Until(ExpectedConditions.AlertIsPresent());
            var alert = Driver.SwitchTo().Alert();
            Assert.AreEqual("������������ �� ������", alert.Text);
            alert.Accept();
        }

        [TestMethod]
        public void CheckPersonalRunner()
        {
            User user = new User();
            Authorization(user);
            Driver.Navigate().GoToUrl("https://localhost:44356/Runners");
            var select = new SelectElement(Driver.FindElement(By.XPath("//*[@id='runner_find_type']")));
            select.SelectByText("��");
            Driver.FindElement(By.XPath("//*[@id='cell_type_id']/a/span")).Click();
            var tableRows = Driver.FindElements(By.XPath("//*[@id='table_test_runners']/tbody/tr"));
            Assert.AreNotEqual(0, tableRows.Count);
            
            foreach (var row in tableRows)
            {
                try
                {
                    row.FindElement(By.LinkText(user.MachineName));
                    return;
                }
                catch { continue; }
            }
        }
        
        [TestMethod]
        public void CheckVisibility()
        { 
            Driver.Navigate().GoToUrl("https://localhost:44356");
            var locator = By.XPath("//*[@id='custom-head']//*[text()='�������']");
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            //Assert below
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(locator));
        }

        [TestMethod]
        public void CheckLinkMatch()
        {
            Authorization();
            Driver.Navigate().GoToUrl("https://localhost:44356/cases");
            Driver.FindElement(By.XPath("//*[@id='cell_type_id']/a/*[text()='�����']/..")).Click();
            var firstTableRow = Driver.FindElement(By.XPath("//*[@id='table_test_case']/tbody/tr[1]"));
            var caseId = firstTableRow.GetAttribute("class").Replace("case_", "");
            Driver.ExecuteJavaScript("window.open()");
            Driver.SwitchTo().Window(Driver.WindowHandles.Last());
            var prefix = $"Case?id={caseId}&projectId=1";
            Driver.Navigate().GoToUrl("https://localhost:44356/" + prefix);
            var actualCaseId = Driver.FindElement(By.XPath("//*[@id='cell_type_id']/label[text()='AutomationId']/../a")).Text;
            Assert.AreEqual(caseId, actualCaseId);
        }
    }
}
